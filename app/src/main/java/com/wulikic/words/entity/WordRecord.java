package com.wulikic.words.entity;

/**
 * Created by vesna on 27/07/2017.
 */

public class WordRecord {

    private String word;
    private int count;

    public WordRecord(String word, int count) {
        this.word = word;
        this.count = count;
    }

    public String getWord() {
        return word;
    }

    public int getCount() {
        return count;
    }
}
