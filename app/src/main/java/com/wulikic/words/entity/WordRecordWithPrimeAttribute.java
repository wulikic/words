package com.wulikic.words.entity;

/**
 * Created by vesna on 27/07/2017.
 */

public class WordRecordWithPrimeAttribute {

    private WordRecord wordRecord;
    private boolean isCountNumberPrime;

    public WordRecordWithPrimeAttribute(WordRecord wordRecord, boolean isCountNumberPrime) {
        this.wordRecord = wordRecord;
        this.isCountNumberPrime = isCountNumberPrime;
    }

    public WordRecord getWordRecord() {
        return wordRecord;
    }

    public boolean isCountNumberPrime() {
        return isCountNumberPrime;
    }

}
