package com.wulikic.words.presentation;

import android.util.Log;

import com.wulikic.words.entity.WordRecordWithPrimeAttribute;
import com.wulikic.words.interactor.prime_number.PrimeWordDetector;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Action;
import io.reactivex.functions.Consumer;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by vesna on 29/07/2017.
 */

class MainPresenter implements MainContract.Presenter {

    private MainContract.View view;
    private PrimeWordDetector primeWordDetectorRx;
    private PrimeWordDetector primeWordDetectorHashMap;
    private DisposableObserver disposableObserver;
    private long timestampStart;
    private final List<WordRecordWithPrimeAttribute> words = new ArrayList<>();
    private long rxSpeed;
    private long hashMapSpeed;

    MainPresenter(PrimeWordDetector primeWordDetectorRx, PrimeWordDetector primeWordDetectorHashMap) {
        this.primeWordDetectorRx = primeWordDetectorRx;
        this.primeWordDetectorHashMap = primeWordDetectorHashMap;
    }

    @Override
    public void loadListUsingRx() {
        loadList(primeWordDetectorRx, new ShowSpeed() {
            @Override
            public void show(long milliseconds) {
                rxSpeed = milliseconds;
                if (view != null) {
                    view.showRxSpeed(milliseconds);
                }
            }
        });
    }

    @Override
    public void loadListUsingHashMap() {
        loadList(primeWordDetectorHashMap, new ShowSpeed() {
            @Override
            public void show(long milliseconds) {
                hashMapSpeed = milliseconds;
                if (view != null) {
                    view.showHashSpeed(milliseconds);
                }
            }
        });
    }

    @Override
    public void attach(MainContract.View view) {
        this.view = view;
        view.showWords(words);
        view.showRxSpeed(rxSpeed);
        view.showHashSpeed(hashMapSpeed);
        if (disposableObserver != null && !disposableObserver.isDisposed()) {
            view.disableSubmitButton();
        } else {
            view.enableSubmitButton();
        }
    }

    @Override
    public void detach(MainContract.View view) {
        this.view = null;
    }

    @Override
    public void terminate() {
        if (disposableObserver != null && !disposableObserver.isDisposed()) {
            disposableObserver.dispose();
        }
    }

    private void loadList(PrimeWordDetector primeWordDetector, final ShowSpeed showSpeed) {
        if (disposableObserver != null && !disposableObserver.isDisposed()) {
            Log.w("MainPresenter", "disposable not disposed and there is nee request");
        }
        disposableObserver = primeWordDetector.createObservable()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe(new Consumer<Disposable>() {
                    @Override
                    public void accept(@NonNull Disposable disposable) throws Exception {
                        if (view != null) {
                            view.disableSubmitButton();
                        }
                        timestampStart = System.currentTimeMillis();
                    }
                })
                .doOnTerminate(new Action() {
                    @Override
                    public void run() throws Exception {
                        long time = System.currentTimeMillis() - timestampStart;
                        showSpeed.show(time);
                        if (view != null) {
                            view.enableSubmitButton();

                        }
                        disposableObserver.dispose();
                    }
                })
                .subscribeWith(new DisposableObserver<List<WordRecordWithPrimeAttribute>>() {
                    @Override
                    public void onNext(@NonNull List<WordRecordWithPrimeAttribute> wordRecordWithPrimeAttributes) {
                        words.clear();
                        words.addAll(wordRecordWithPrimeAttributes);
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        if (view != null) {

                        }
                    }

                    @Override
                    public void onComplete() {
                        if (view != null) {
                            view.showWords(words);
                        }
                    }
                });
    }

    interface ShowSpeed {
        void show(long milliseconds);
    }

}
