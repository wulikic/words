package com.wulikic.words.presentation;

import com.wulikic.words.entity.WordRecordWithPrimeAttribute;

import java.util.List;

/**
 * Created by vesna on 29/07/2017.
 */

class MainContract {

    interface View {
        void showWords(List<WordRecordWithPrimeAttribute> words);
        void showRxSpeed(long milliseconds);
        void showHashSpeed(long milliseconds);
        void enableSubmitButton();
        void disableSubmitButton();
    }

    interface Presenter {
        void loadListUsingRx();
        void loadListUsingHashMap();
        void attach(View view);
        void detach(View view);
        void terminate();
    }

}
