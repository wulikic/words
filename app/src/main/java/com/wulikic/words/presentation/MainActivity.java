package com.wulikic.words.presentation;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.wulikic.words.R;
import com.wulikic.words.entity.WordRecordWithPrimeAttribute;
import com.wulikic.words.interactor.prime_number.PrimeWordDetector;

import java.util.List;
import java.util.Locale;

import javax.inject.Inject;
import javax.inject.Named;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity implements MainContract.View {

    @BindView(R.id.list_container)
    RecyclerView recyclerView;

    @BindView(R.id.go)
    Button goButton;

    @BindView(R.id.radio_group)
    RadioGroup radioGroup;

    @BindView(R.id.rx_speed)
    TextView rxSpeed;

    @BindView(R.id.hashmap_speed)
    TextView hashMapSpeed;

    private WordListAdapter adapter;

    @Inject
    @Named("RxPrime")
    PrimeWordDetector primeWordDetectorRx;

    @Inject
    @Named("HashMapPrime")
    PrimeWordDetector primeWordDetectorHashMap;

    private MainContract.Presenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ((WordsApplication)getApplication()).getApplicationComponent().inject(this);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        goButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int checkedButtonId = radioGroup.getCheckedRadioButtonId();
                switch (checkedButtonId) {
                    case R.id.rx: presenter.loadListUsingRx(); break;
                    case R.id.hashmap: presenter.loadListUsingHashMap(); break;
                    default: break;
                }
            }
        });

        recyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        adapter = new WordListAdapter();
        recyclerView.setAdapter(adapter);

        if (savedInstanceState == null) {
            presenter = new MainPresenter(primeWordDetectorRx, primeWordDetectorHashMap);
        } else {
            presenter = (MainContract.Presenter) getLastCustomNonConfigurationInstance();
        }
        presenter.attach(this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (isFinishing()) {
            presenter.terminate();
        } else {
            presenter.detach(this);
        }
    }

    @Override
    public void showWords(List<WordRecordWithPrimeAttribute> words) {
        adapter.setItems(words);
    }

    @Override
    public void showRxSpeed(long milliseconds) {
        rxSpeed.setText(String.format(Locale.UK, "%d ms", milliseconds));
    }

    @Override
    public void showHashSpeed(long milliseconds) {
        hashMapSpeed.setText(String.format(Locale.UK, "%d ms", milliseconds));
    }

    @Override
    public void enableSubmitButton() {
        goButton.setEnabled(true);
    }

    @Override
    public void disableSubmitButton() {
        goButton.setEnabled(false);
    }

    @Override
    public Object onRetainCustomNonConfigurationInstance() {
        return presenter;
    }

}
