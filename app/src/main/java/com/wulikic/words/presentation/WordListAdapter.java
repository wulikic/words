package com.wulikic.words.presentation;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.wulikic.words.R;
import com.wulikic.words.entity.WordRecordWithPrimeAttribute;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by vesna on 29/07/2017.
 */

class WordListAdapter extends RecyclerView.Adapter<WordListAdapter.WordHolder> {

    private List<WordRecordWithPrimeAttribute> items;

    public void setItems(List<WordRecordWithPrimeAttribute> items) {
        this.items = items;
        notifyDataSetChanged();
    }

    @Override
    public WordHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.word_item, parent, false);
        return new WordHolder(view);
    }

    @Override
    public void onBindViewHolder(WordHolder holder, int position) {
        WordRecordWithPrimeAttribute item = items.get(position);
        holder.word.setText(item.getWordRecord().getWord());
        holder.count.setText(String.valueOf(item.getWordRecord().getCount()));
        holder.prime.setText(String.valueOf(item.isCountNumberPrime()));
    }

    @Override
    public int getItemCount() {
        return items != null ? items.size() : 0;
    }

    static class WordHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.word)
        TextView word;

        @BindView(R.id.count)
        TextView count;

        @BindView(R.id.prime)
        TextView prime;

        WordHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

}
