package com.wulikic.words.presentation;

import android.app.Application;

import com.wulikic.words.di.component.ApplicationComponent;
import com.wulikic.words.di.component.DaggerApplicationComponent;
import com.wulikic.words.di.module.DataSourceModule;
import com.wulikic.words.di.module.InteractorModule;
import com.wulikic.words.di.module.RepositoryModule;
import com.wulikic.words.di.module.TokenizerModule;
import com.wulikic.words.di.module.WordCountModule;

/**
 * Created by vesna on 29/07/2017.
 */

public class WordsApplication extends Application {

    private ApplicationComponent applicationComponent;

    @Override
    public void onCreate() {
        super.onCreate();
        applicationComponent = DaggerApplicationComponent.builder()
                .dataSourceModule(new DataSourceModule(this))
                .repositoryModule(new RepositoryModule())
                .wordCountModule(new WordCountModule())
                .tokenizerModule(new TokenizerModule())
                .interactorModule(new InteractorModule())
                .build();
    }

    public ApplicationComponent getApplicationComponent() {
        return applicationComponent;
    }
}
