package com.wulikic.words.di.module;

import android.content.res.AssetManager;
import android.support.annotation.NonNull;

import com.wulikic.words.data.Tokenizer;
import com.wulikic.words.data.WordsAssetsRepository;
import com.wulikic.words.data.WordsRepository;

import javax.inject.Named;

import dagger.Module;
import dagger.Provides;

/**
 * Created by vesna on 29/07/2017.
 */

@Module
public class RepositoryModule {

    @Provides
    WordsRepository provideWordsRepository(@NonNull AssetManager assetManager, @NonNull @Named("sourceFile") String fileName, @NonNull Tokenizer tokenizer) {
        return new WordsAssetsRepository(assetManager, fileName, tokenizer);
    }

}
