package com.wulikic.words.di.module;

import android.content.Context;
import android.content.res.AssetManager;

import javax.inject.Named;

import dagger.Module;
import dagger.Provides;

/**
 * Created by vesna on 29/07/2017.
 */
@Module
public class DataSourceModule {

    private Context context;

    public DataSourceModule(Context context) {
        this.context = context;
    }

    @Provides
    AssetManager provideSource() {
        return context.getAssets();
    }

    @Provides
    @Named("sourceFile")
    String provideFileName() {
        return "Railway-Children-by-E-Nesbit.txt";
    }

}
