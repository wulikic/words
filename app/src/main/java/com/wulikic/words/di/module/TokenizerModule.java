package com.wulikic.words.di.module;

import com.wulikic.words.data.Tokenizer;
import com.wulikic.words.data.TokenizerImpl;

import dagger.Module;
import dagger.Provides;

/**
 * Created by vesna on 30/07/2017.
 */

@Module
public class TokenizerModule {

    @Provides
    Tokenizer provideTokenizer() {
        return new TokenizerImpl();
    }
}
