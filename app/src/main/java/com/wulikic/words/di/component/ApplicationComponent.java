package com.wulikic.words.di.component;

import com.wulikic.words.di.module.DataSourceModule;
import com.wulikic.words.di.module.InteractorModule;
import com.wulikic.words.di.module.RepositoryModule;
import com.wulikic.words.di.module.TokenizerModule;
import com.wulikic.words.di.module.WordCountModule;
import com.wulikic.words.presentation.MainActivity;

import dagger.Component;

/**
 * Created by vesna on 27/07/2017.
 */

@Component(modules = {
        InteractorModule.class,
        DataSourceModule.class,
        TokenizerModule.class,
        RepositoryModule.class,
        WordCountModule.class
})
public interface ApplicationComponent {

    void inject(MainActivity mainActivity);
}
