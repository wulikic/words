package com.wulikic.words.di.module;

import android.support.annotation.NonNull;

import com.wulikic.words.data.WordsRepository;
import com.wulikic.words.interactor.prime_number.PrimeNumberChecker;
import com.wulikic.words.interactor.prime_number.PrimeNumberCheckerImpl;
import com.wulikic.words.interactor.prime_number.PrimeNumberCheckerWithCache;
import com.wulikic.words.interactor.prime_number.PrimeWordDetector;
import com.wulikic.words.interactor.prime_number.PrimeWordDetectorImpl;
import com.wulikic.words.interactor.word_count.WordCounter;
import com.wulikic.words.interactor.word_count.WordCounterRx;

import javax.inject.Named;

import dagger.Module;
import dagger.Provides;

/**
 * Created by vesna on 29/07/2017.
 */

@Module
public class InteractorModule {

    @Provides
    PrimeWordDetector provideWordCounterWithPrimesInteractor(@NonNull WordCounter wordCounter, @NonNull PrimeNumberChecker primeNumberChecker) {
        return new PrimeWordDetectorImpl(wordCounter, primeNumberChecker);
    }

    @Provides
    WordCounter provideWordCounter(@NonNull WordsRepository wordsRepository) {
        return new WordCounterRx(wordsRepository);
    }

    @Provides
    @Named("no_cache")
    PrimeNumberChecker providePrimeNumberChecker() {
        return new PrimeNumberCheckerImpl();
    }

    @Provides
    @Named("with_cache")
    PrimeNumberChecker providePrimeNumberCheckerWithCache() {
        return new PrimeNumberCheckerWithCache(new PrimeNumberCheckerImpl(), 10000);
    }

}
