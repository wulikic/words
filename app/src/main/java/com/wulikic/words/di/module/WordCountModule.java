package com.wulikic.words.di.module;

import com.wulikic.words.data.WordsRepository;
import com.wulikic.words.interactor.prime_number.PrimeNumberChecker;
import com.wulikic.words.interactor.prime_number.PrimeWordDetector;
import com.wulikic.words.interactor.prime_number.PrimeWordDetectorImpl;
import com.wulikic.words.interactor.word_count.WordCounterHashMap;
import com.wulikic.words.interactor.word_count.WordCounterRx;

import javax.inject.Named;

import dagger.Module;
import dagger.Provides;

/**
 * Created by vesna on 29/07/2017.
 */

@Module
public class WordCountModule {

    @Provides
    public WordCounterRx provideWordCounterRx(WordsRepository wordsRepository) {
        return new WordCounterRx(wordsRepository);
    }

    @Provides
    public WordCounterHashMap provideWordCounterHasMap(WordsRepository wordsRepository) {
        return new WordCounterHashMap(wordsRepository);
    }

    @Provides
    @Named("RxPrime")
    public PrimeWordDetector primeWordDetectorRx(WordCounterRx wordCounterRx, @Named("no_cache") PrimeNumberChecker primeNumberChecker) {
        return new PrimeWordDetectorImpl(wordCounterRx, primeNumberChecker);
    }

    @Provides
    @Named("HashMapPrime")
    public PrimeWordDetector primeWordDetectorHash(WordCounterHashMap wordCounterHashMap, @Named("with_cache") PrimeNumberChecker primeNumberChecker) {
        return new PrimeWordDetectorImpl(wordCounterHashMap, primeNumberChecker);
    }

}
