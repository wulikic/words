package com.wulikic.words.data;

import java.util.List;

/**
 * Created by vesna on 30/07/2017.
 */

public interface Tokenizer {

    List<String> words(String text);
}
