package com.wulikic.words.data;

import io.reactivex.Observable;

/**
 * Created by vesna on 27/07/2017.
 */

public interface WordsRepository {

    Observable<String> words();

}
