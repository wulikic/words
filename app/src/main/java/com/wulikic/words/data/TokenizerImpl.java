package com.wulikic.words.data;

import android.support.annotation.NonNull;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by vesna on 30/07/2017.
 */

public class TokenizerImpl implements Tokenizer {

    @Override
    public List<String> words(@NonNull String text) {
        String[] tokens = text.split("\\W");
        List<String> list = new ArrayList<String>(tokens.length);
        for (String token : tokens) {
            String word = token.trim();
            if (word.length() > 0) {
                list.add(word.toLowerCase());
            }
        }
        return list;
    }

}
