package com.wulikic.words.data;

import android.content.res.AssetManager;
import android.support.annotation.NonNull;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.List;

import io.reactivex.Observable;
import io.reactivex.ObservableEmitter;
import io.reactivex.ObservableOnSubscribe;

/**
 * Created by vesna on 27/07/2017.
 */

public class WordsAssetsRepository implements WordsRepository {

    @NonNull
    private final AssetManager assetManager;

    @NonNull
    private final String fileName;

    @NonNull
    private final Tokenizer tokenizer;

    public WordsAssetsRepository(@NonNull AssetManager assetManager, @NonNull String fileName, @NonNull Tokenizer tokenizer) {
        this.assetManager = assetManager;
        this.fileName = fileName;
        this.tokenizer = tokenizer;
    }

    @Override
    public Observable<String> words() {
        return Observable.create(new ObservableOnSubscribe<String>() {
            @Override
            public void subscribe(@io.reactivex.annotations.NonNull ObservableEmitter<String> e) throws Exception {
                InputStream stream = null;
                InputStreamReader inputStreamReader = null;
                BufferedReader bufferedReader = null;
                try {
                    stream = assetManager.open(fileName);
                    inputStreamReader = new InputStreamReader(stream);
                    bufferedReader = new BufferedReader(inputStreamReader);
                    String line = null;
                    while ((line = bufferedReader.readLine()) != null) {
                        List<String> words = tokenizer.words(line);
                        for (String word : words) {
                            e.onNext(word);
                        }
                    }
                    e.onComplete();
                } catch (FileNotFoundException error) {
                    e.onError(error);
                }
                finally {
                    if (stream != null) {
                        stream.close();
                    }
                    if (inputStreamReader != null) {
                        inputStreamReader.close();
                    }
                    if (bufferedReader != null) {
                        bufferedReader.close();
                    }
                }
            }
        });
    }

}
