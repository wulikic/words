package com.wulikic.words.interactor.prime_number;

/**
 * Created by vesna on 29/07/2017.
 */

/**
 *  number is a prime number if is greater then 1 and has no positive divisors other than 1 and itself.
 *  zero and negative numbers are not supported
 */
public interface PrimeNumberChecker {

    boolean isPrime(int number) throws UnsupportedNumberException;

}
