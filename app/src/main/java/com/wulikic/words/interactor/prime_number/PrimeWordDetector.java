package com.wulikic.words.interactor.prime_number;

import com.wulikic.words.entity.WordRecordWithPrimeAttribute;

import java.util.List;

import io.reactivex.Observable;

/**
 * Created by vesna on 27/07/2017.
 */

public interface PrimeWordDetector {

    Observable<List<WordRecordWithPrimeAttribute>> createObservable();

}
