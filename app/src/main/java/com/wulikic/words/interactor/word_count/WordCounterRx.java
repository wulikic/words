package com.wulikic.words.interactor.word_count;

import android.support.annotation.NonNull;

import com.wulikic.words.data.WordsRepository;
import com.wulikic.words.entity.WordRecord;

import java.util.List;

import io.reactivex.Observable;
import io.reactivex.ObservableSource;
import io.reactivex.functions.Function;
import io.reactivex.functions.Predicate;
import io.reactivex.observables.GroupedObservable;

/**
 * Created by vesna on 27/07/2017.
 */

public class WordCounterRx implements WordCounter {

    @NonNull
    private final WordsRepository wordsRepository;

    public WordCounterRx(@NonNull WordsRepository wordsRepository) {
        this.wordsRepository = wordsRepository;
    }

    @Override
    public Observable<List<WordRecord>> createObservable() {
        return wordsRepository.words().groupBy(new Function<String, String>() {
            @Override
            public String apply(@io.reactivex.annotations.NonNull String s) throws Exception {
                return s;
            }
        }).concatMap(new Function<GroupedObservable<String, String>, ObservableSource<WordRecord>>() {
            @Override
            public ObservableSource<WordRecord> apply(@io.reactivex.annotations.NonNull final GroupedObservable<String, String> stringStringGroupedObservable) throws Exception {
                return stringStringGroupedObservable.count().map(new Function<Long, WordRecord>() {
                    @Override
                    public WordRecord apply(@io.reactivex.annotations.NonNull Long aLong) throws Exception {
                        int count = aLong.intValue();
                        return new WordRecord(stringStringGroupedObservable.getKey(), count);
                    }
                }).toObservable();
            }
        }).toList().toObservable().filter(new Predicate<List<WordRecord>>() {
            @Override
            public boolean test(@io.reactivex.annotations.NonNull List<WordRecord> wordAppearances) throws Exception {
                return wordAppearances.size() > 0;
            }
        });
    }

}
