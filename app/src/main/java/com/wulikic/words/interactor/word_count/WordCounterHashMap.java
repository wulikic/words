package com.wulikic.words.interactor.word_count;

import android.support.annotation.NonNull;

import com.wulikic.words.data.WordsRepository;
import com.wulikic.words.entity.WordRecord;

import java.util.HashMap;
import java.util.List;

import io.reactivex.Observable;
import io.reactivex.functions.Function;
import io.reactivex.functions.Predicate;

/**
 * Created by vesna on 29/07/2017.
 */

public class WordCounterHashMap implements WordCounter {

    @NonNull
    private final WordsRepository wordsRepository;

    public WordCounterHashMap(@NonNull WordsRepository wordsRepository) {
        this.wordsRepository = wordsRepository;
    }

    @Override
    public Observable<List<WordRecord>> createObservable() {
        final HashMap<String, Integer> wordCountMap = new HashMap<>();
        return  wordsRepository.words()
                .filter(new Predicate<String>() {
                    @Override
                    public boolean test(@io.reactivex.annotations.NonNull String word) throws Exception {
                            int count;
                            if (wordCountMap.containsKey(word)) {
                                count = wordCountMap.get(word) + 1;
                            } else {
                                count = 1;
                            }
                            wordCountMap.put(word, count);
                            return false;
                        }
                    })
                .concatWith(Observable.just(wordCountMap).flatMapIterable(new Function<HashMap<String, Integer>, Iterable<String>>() {
                    @Override
                    public Iterable<String> apply(@io.reactivex.annotations.NonNull HashMap<String, Integer> wordCountHashMap) throws Exception {
                        return wordCountHashMap.keySet();
                    }
                }))
                .map(new Function<String, WordRecord>() {
                    @Override
                    public WordRecord apply(@io.reactivex.annotations.NonNull String s) throws Exception {
                        return new WordRecord(s, wordCountMap.get(s));
                    }
                }).toList().toObservable()
                        .filter(new Predicate<List<WordRecord>>() {
                            @Override
                            public boolean test(@io.reactivex.annotations.NonNull List<WordRecord> wordAppearances) throws Exception {
                                return wordAppearances.size() > 0;
                            }
                });

    }



}
