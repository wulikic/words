package com.wulikic.words.interactor.prime_number;

import android.support.annotation.NonNull;

import com.wulikic.words.entity.WordRecord;
import com.wulikic.words.entity.WordRecordWithPrimeAttribute;
import com.wulikic.words.interactor.word_count.WordCounter;

import java.util.List;

import io.reactivex.Observable;
import io.reactivex.functions.Function;

/**
 * Created by vesna on 28/07/2017.
 */

public class PrimeWordDetectorImpl implements PrimeWordDetector {

    @NonNull
    private final WordCounter wordCounter;

    @NonNull
    private final PrimeNumberChecker primeNumberChecker;

    public PrimeWordDetectorImpl(@NonNull WordCounter wordCounter, @NonNull PrimeNumberChecker primeNumberChecker) {
        this.wordCounter = wordCounter;
        this.primeNumberChecker = primeNumberChecker;
    }

    @Override
    public Observable<List<WordRecordWithPrimeAttribute>> createObservable() {
        return wordCounter.createObservable()
                .flatMapIterable(new Function<List<WordRecord>, Iterable<WordRecord>>() {
                    @Override
                    public Iterable<WordRecord> apply(@io.reactivex.annotations.NonNull List<WordRecord> wordAppearances) throws Exception {
                        return wordAppearances;
                    }
                })
                .map(new Function<WordRecord, WordRecordWithPrimeAttribute>() {
                    @Override
                    public WordRecordWithPrimeAttribute apply(@io.reactivex.annotations.NonNull WordRecord wordRecord) throws Exception {
                        boolean isPrime = primeNumberChecker.isPrime(wordRecord.getCount());
                        return new WordRecordWithPrimeAttribute(wordRecord, isPrime);
                    }
                })
                .toList()
                .toObservable();
    }

}
