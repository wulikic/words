package com.wulikic.words.interactor.word_count;

import com.wulikic.words.entity.WordRecord;

import java.util.List;

import io.reactivex.Observable;

/**
 * Created by vesna on 27/07/2017.
 */

public interface WordCounter {

    Observable<List<WordRecord>> createObservable();
}
