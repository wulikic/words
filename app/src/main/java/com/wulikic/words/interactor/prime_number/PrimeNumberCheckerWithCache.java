package com.wulikic.words.interactor.prime_number;

import android.support.annotation.VisibleForTesting;

import java.util.BitSet;

import io.reactivex.annotations.NonNull;

/**
 * Created by vesna on 30/07/2017.
 */

public class PrimeNumberCheckerWithCache implements PrimeNumberChecker {

    private BitSet cache;
    private BitSet isCached;

    private PrimeNumberCheckerImpl primeNumberChecker;
    private int cacheSize;

    public PrimeNumberCheckerWithCache(PrimeNumberCheckerImpl primeNumberChecker, int cacheSize) {
        this.primeNumberChecker = primeNumberChecker;
        this.cacheSize = cacheSize;
        cache = new BitSet(cacheSize);
        isCached = new BitSet(cacheSize);
    }

    @Override
    public boolean isPrime(int number) throws UnsupportedNumberException {
        if (isCacheable(number)) {
            if (isValueCached(number)) {
                return getValueFromCache(number);
            } else {
                boolean isPrime = primeNumberChecker.isPrime(number);
                saveInCache(number, isPrime);
                return isPrime;
            }
        } else {
            return primeNumberChecker.isPrime(number);
        }
    }

    @VisibleForTesting(otherwise = VisibleForTesting.PRIVATE)
    protected boolean isCacheable(int number) {
        return number > 0 && number < cacheSize;
    }

    @VisibleForTesting(otherwise = VisibleForTesting.PRIVATE)
    protected boolean getValueFromCache(int number) {
        return cache.get(number);
    }

    @VisibleForTesting(otherwise = VisibleForTesting.PRIVATE)
    protected boolean isValueCached(int number) {
        return isCached.get(number);

    }

    @VisibleForTesting(otherwise = VisibleForTesting.PRIVATE)
    protected void saveInCache(int number, boolean value) {
        cache.set(number, value);
        isCached.set(number, true);
    }

}
