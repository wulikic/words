package com.wulikic.words.interactor.prime_number;

/**
 * Created by vesna on 29/07/2017.
 */

class UnsupportedNumberException extends Exception {

    UnsupportedNumberException() {
        super("Only positive numbers are supported");
    }

}
