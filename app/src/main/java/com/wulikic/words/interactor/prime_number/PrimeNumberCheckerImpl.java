package com.wulikic.words.interactor.prime_number;

/**
 * Created by vesna on 29/07/2017.
 */

public class PrimeNumberCheckerImpl implements PrimeNumberChecker {

    @Override
    public boolean isPrime(int number) throws UnsupportedNumberException {
        if (number < 1) {
            throw new UnsupportedNumberException();
        }
        if (number == 1) {
            return false;
        }
        if (number == 2 || number == 3) {
            return true;
        }
        if (number % 2 == 0 || number % 3 == 0) {
            return false;
        }
        for (long i = 5; i * i <= number; i = i + 6) {
            if (number % i == 0 || number % (i + 2) == 0) {
                return false;
            }
        }
        return true;
    }
}
