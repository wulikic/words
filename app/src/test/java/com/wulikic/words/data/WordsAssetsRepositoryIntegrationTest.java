package com.wulikic.words.data;

import android.content.res.AssetManager;

import com.wulikic.words.util.FileUtils;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import java.io.File;
import java.io.FileInputStream;

import io.reactivex.observers.TestObserver;

/**
 * Created by vesna on 30/07/2017.
 */

@RunWith(MockitoJUnitRunner.class)
public class WordsAssetsRepositoryIntegrationTest {

    @Mock
    AssetManager assetManager;

    @Test
    public void testWhenSourceContainsOneWord_shouldReturnThatWord() throws Exception {
        // given
        String fileName = Mockito.anyString();
        File file = FileUtils.getResourceFile("one_word.txt");
        Mockito.when(assetManager.open(fileName)).thenReturn(new FileInputStream(file));
        Tokenizer tokenizer = new TokenizerImpl();
        WordsRepository wordsRepository = new WordsAssetsRepository(assetManager, fileName, tokenizer);

        // when
        TestObserver<String> testObserver = wordsRepository.words().test();

        // then
        testObserver.assertNoErrors();
        testObserver.assertValueCount(1);
        testObserver.assertValue("word");
    }

    @Test
    public void testWhenFileHasOnlyNonWords_shouldReturnNoValues() throws Exception {
        // given
        String fileName = Mockito.anyString();
        File file = FileUtils.getResourceFile("only_spaces_and_punctuation.txt");
        Mockito.when(assetManager.open(fileName)).thenReturn(new FileInputStream(file));
        Tokenizer tokenizer = new TokenizerImpl();
        WordsRepository wordsRepository = new WordsAssetsRepository(assetManager, fileName, tokenizer);

        // when
        TestObserver<String> testObserver = wordsRepository.words().test();

        // then
        testObserver.assertNoErrors();
        testObserver.assertNoValues();
    }

    @Test
    public void testWhenFileHasTwoWords_shouldReturnThoseTwoWords() throws Exception {
        // given
        String fileName = Mockito.anyString();
        File file = FileUtils.getResourceFile("two_words.txt");
        Mockito.when(assetManager.open(fileName)).thenReturn(new FileInputStream(file));
        Tokenizer tokenizer = new TokenizerImpl();
        WordsRepository wordsRepository = new WordsAssetsRepository(assetManager, fileName, tokenizer);

        // when
        TestObserver<String> testObserver = wordsRepository.words().test();

        // then
        testObserver.assertNoErrors();
        testObserver.assertValues("word1", "word2");
    }
}