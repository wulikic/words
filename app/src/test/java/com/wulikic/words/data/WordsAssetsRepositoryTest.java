package com.wulikic.words.data;

import android.content.res.AssetManager;

import com.wulikic.words.util.FileUtils;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;

import io.reactivex.observers.TestObserver;

/**
 * Created by vesna on 30/07/2017.
 */

@RunWith(MockitoJUnitRunner.class)
public class WordsAssetsRepositoryTest {

    @Mock
    Tokenizer tokenizer;

    @Mock
    AssetManager assetManager;

    @Test
    public void testWhenSourceDoesNotExist_shouldReturnFnfException() throws Exception {
        // given
        String fileName = "non_existing_file.txt";
        Mockito.when(assetManager.open(fileName)).thenThrow(new FileNotFoundException());
        WordsRepository wordsRepository = new WordsAssetsRepository(assetManager, fileName, tokenizer);

        // when
        TestObserver<String> testObserver = wordsRepository.words().test();

        // then
        testObserver.assertError(FileNotFoundException.class);
    }

    @Test
    public void testWhenFileEmpty_shouldReturnNoValues() throws Exception {
        // given
        String fileName = Mockito.anyString();
        File file = FileUtils.getResourceFile("empty.txt");
        Mockito.when(assetManager.open(fileName)).thenReturn(new FileInputStream(file));
        WordsRepository wordsRepository = new WordsAssetsRepository(assetManager, fileName, tokenizer);

        // when
        TestObserver<String> testObserver = wordsRepository.words().test();

        // then
        testObserver.assertNoErrors();
        testObserver.assertNoValues();
    }

}