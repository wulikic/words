package com.wulikic.words.data;

import org.junit.Assert;
import org.junit.Test;

import java.util.List;

/**
 * Created by vesna on 30/07/2017.
 */
public class TokenizerImplTest {

    @Test
    public void testGivenEmptyString_shouldReturnNoWords() throws Exception {
        // given
        String text = "";
        Tokenizer tokenizer = new TokenizerImpl();

        // when
        List<String> words = tokenizer.words(text);

        // then
        Assert.assertEquals(0, words.size());
    }

    @Test
    public void testGivenNonWordString_shouldReturnNoWords() throws Exception {
        // given
        String text = ", ; !%";
        Tokenizer tokenizer = new TokenizerImpl();

        // when
        List<String> words = tokenizer.words(text);

        // then
        Assert.assertEquals(0, words.size());
    }

    @Test
    public void testGivenWordUnderQuotes_shouldReturnThatWordWithoutQuotes() throws Exception {
        // given
        String text = "'once'";
        Tokenizer tokenizer = new TokenizerImpl();

        // when
        List<String> words = tokenizer.words(text);

        // then
        Assert.assertEquals(1, words.size());
        Assert.assertEquals("once", words.get(0));
    }

    @Test
    public void testGivenNumber_shouldReturnThatNumber() throws Exception {
        // given
        String text = "3";
        Tokenizer tokenizer = new TokenizerImpl();

        // when
        List<String> words = tokenizer.words(text);

        // then
        Assert.assertEquals(1, words.size());
        Assert.assertEquals("3", words.get(0));
    }

}