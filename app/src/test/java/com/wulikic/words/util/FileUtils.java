package com.wulikic.words.util;

import java.io.File;

/**
 * Created by vesna on 29/07/2017.
 */

public class FileUtils {

    public static File getResourceFile(String name) {
        return new File("app/src/test/java/com/wulikic/words/resources", name);
    }
}
