package com.wulikic.words.interactor.prime_number;

/**
 * Created by vesna on 30/07/2017.
 */
public class PrimeNumberCheckerWithCacheIntegrationTest extends PrimeNumberCheckerTest {

    @Override
    protected PrimeNumberChecker getPrimeNumberCheckerImplementation() {
        return new PrimeNumberCheckerWithCache(new PrimeNumberCheckerImpl(), 1000);
    }

}