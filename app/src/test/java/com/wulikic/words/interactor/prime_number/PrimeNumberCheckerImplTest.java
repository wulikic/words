package com.wulikic.words.interactor.prime_number;

/**
 * Created by vesna on 29/07/2017.
 */
public class PrimeNumberCheckerImplTest extends PrimeNumberCheckerTest {

    @Override
    protected PrimeNumberChecker getPrimeNumberCheckerImplementation() {
        return new PrimeNumberCheckerImpl();
    }
}