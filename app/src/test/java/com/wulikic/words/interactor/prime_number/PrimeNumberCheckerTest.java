package com.wulikic.words.interactor.prime_number;

import org.junit.Assert;
import org.junit.Test;

/**
 * Created by vesna on 29/07/2017.
 */
public abstract class PrimeNumberCheckerTest {

    protected abstract PrimeNumberChecker getPrimeNumberCheckerImplementation();

    @Test(expected = UnsupportedNumberException.class)
    public void testGivenNegativeNumber_shouldThrowException() throws Exception {
        // given
        int number = -3;

        // when
        PrimeNumberChecker primeNumberChecker = getPrimeNumberCheckerImplementation();
        boolean isPrime = primeNumberChecker.isPrime(number);
    }

    @Test(expected = UnsupportedNumberException.class)
    public void testGivenNumberZero_shouldThrowException() throws Exception {
        // given
        int number = 0;

        // when
        PrimeNumberChecker primeNumberChecker = getPrimeNumberCheckerImplementation();
        boolean isPrime = primeNumberChecker.isPrime(number);
    }

    @Test
    public void testGivenNumber1_shouldReturnFalse() throws Exception {
        // given
        int number = 1;

        // when
        PrimeNumberChecker primeNumberChecker = getPrimeNumberCheckerImplementation();
        boolean isPrime = primeNumberChecker.isPrime(number);

        // then
        Assert.assertFalse(isPrime);
    }

    @Test
    public void testGivenNumber2_shouldReturnTrue() throws Exception {
        // given
        int number = 2;

        // when
        PrimeNumberChecker primeNumberChecker = getPrimeNumberCheckerImplementation();
        boolean isPrime = primeNumberChecker.isPrime(number);

        // then
        Assert.assertTrue(isPrime);
    }

    @Test
    public void testGivenNumber3_shouldReturnTrue() throws Exception {
        // given
        int number = 3;

        // when
        PrimeNumberChecker primeNumberChecker = getPrimeNumberCheckerImplementation();
        boolean isPrime = primeNumberChecker.isPrime(number);

        // then
        Assert.assertTrue(isPrime);
    }

    @Test
    public void testGivenNumber4_shouldReturnFalse() throws Exception {
        // given
        int number = 4;

        // when
        PrimeNumberChecker primeNumberChecker = getPrimeNumberCheckerImplementation();
        boolean isPrime = primeNumberChecker.isPrime(number);

        // then
        Assert.assertFalse(isPrime);
    }

    @Test
    public void testGivenNumber5_shouldReturnTrue() throws Exception {
        // given
        int number = 5;

        // when
        PrimeNumberChecker primeNumberChecker = getPrimeNumberCheckerImplementation();
        boolean isPrime = primeNumberChecker.isPrime(number);

        // then
        Assert.assertTrue(isPrime);
    }

    @Test
    public void testGivenNumber499_shouldReturnTrue() throws Exception {
        // given
        int number = 499;

        // when
        PrimeNumberChecker primeNumberChecker = getPrimeNumberCheckerImplementation();
        boolean isPrime = primeNumberChecker.isPrime(number);

        // then
        Assert.assertTrue(isPrime);
    }

    @Test
    public void testGivenNumber121_shouldReturnFalse() throws Exception {
        // given
        int number = 121;

        // when
        PrimeNumberChecker primeNumberChecker = getPrimeNumberCheckerImplementation();
        boolean isPrime = primeNumberChecker.isPrime(number);

        // then
        Assert.assertFalse(isPrime);
    }

}