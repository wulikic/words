package com.wulikic.words.interactor.prime_number;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

/**
 * Created by vesna on 30/07/2017.
 */

@RunWith(MockitoJUnitRunner.class)
public class PrimeNumberCheckerWithCacheCacheTest {

    @Mock
    PrimeNumberCheckerImpl primeNumberCheckerImpl;

    @Test
    public void testIfCacheSizeZero_shouldCalculateIsPrime() throws Exception {
        // given
        int number = 4;
        int cacheSize = 0;
        PrimeNumberCheckerWithCache primeNumberCheckerWithCache = new PrimeNumberCheckerWithCache(primeNumberCheckerImpl, cacheSize);

        // when
        boolean isPrime = primeNumberCheckerWithCache.isPrime(number);

        // then
        verify(primeNumberCheckerImpl).isPrime(number);
    }

    @Test
    public void testIfGivenNumberBiggerThenCacheSize_shouldCalculateIsPrime() throws Exception {
        // given
        int number = 4;
        int cacheSize = 3;
        PrimeNumberCheckerWithCache primeNumberCheckerWithCache = new PrimeNumberCheckerWithCache(primeNumberCheckerImpl, cacheSize);

        // when
        boolean isPrime = primeNumberCheckerWithCache.isPrime(number);

        // then
        verify(primeNumberCheckerImpl).isPrime(number);
    }

    @Test
    public void testIfGivenNumberLessThenCacheSizeButNotInCache_shouldCalculateIsPrime() throws Exception {
        // given
        int number = 2;
        int cacheSize = 3;
        PrimeNumberCheckerWithCache primeNumberCheckerWithCache = spy(new PrimeNumberCheckerWithCache(primeNumberCheckerImpl, cacheSize));
        doReturn(false).when(primeNumberCheckerWithCache).isValueCached(number);

        // when
        boolean isPrime = primeNumberCheckerWithCache.isPrime(number);

        // then
        verify(primeNumberCheckerImpl).isPrime(number);
    }

    @Test
    public void testIfGivenNumberLessThenCacheSizeAndInCache_shouldNotCalculateIsPrime() throws Exception {
        // given
        int number = 2;
        int cacheSize = 3;
        PrimeNumberCheckerWithCache primeNumberCheckerWithCache = spy(new PrimeNumberCheckerWithCache(primeNumberCheckerImpl, cacheSize));
        doReturn(true).when(primeNumberCheckerWithCache).isValueCached(number);

        // when
        boolean isPrime = primeNumberCheckerWithCache.isPrime(number);

        // then
        verify(primeNumberCheckerImpl, times(0)).isPrime(number);
    }

}
