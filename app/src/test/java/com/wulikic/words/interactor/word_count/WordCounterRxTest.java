package com.wulikic.words.interactor.word_count;

import com.wulikic.words.data.WordsRepository;

/**
 * Created by vesna on 27/07/2017.
 */
public class WordCounterRxTest extends WordCounterTest{


    @Override
    protected WordCounter getWordCounterImplementation(WordsRepository repository) {
        return new WordCounterRx(repository);
    }

}