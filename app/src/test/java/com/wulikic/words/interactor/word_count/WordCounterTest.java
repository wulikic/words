package com.wulikic.words.interactor.word_count;

import com.wulikic.words.data.WordsRepository;
import com.wulikic.words.entity.WordRecord;

import org.junit.Assert;
import org.junit.Test;

import java.util.List;

import io.reactivex.Observable;
import io.reactivex.observers.TestObserver;

/**
 * Created by vesna on 29/07/2017.
 */

public abstract class WordCounterTest {

    protected abstract WordCounter getWordCounterImplementation(WordsRepository repository);

    @Test
    public void testGivenNoWordsReceived_shouldReturnNoValues() throws Exception {
        // given
        WordsRepository repository = new WordsRepository() {
            @Override
            public Observable<String> words() {
                return Observable.empty();
            }
        };

        // when
        WordCounter wordCounter = getWordCounterImplementation(repository);
        TestObserver<List<WordRecord>> testObserver = wordCounter.createObservable().test();

        // then
        testObserver.assertNoErrors();
        testObserver.assertNoValues();
    }

    @Test
    public void testGivenOnlyOneWordReceived_shouldReturnThatWordWithValueOne() throws Exception {
        // given
        WordsRepository repository = new WordsRepository() {
            @Override
            public Observable<String> words() {
                return Observable.just("word");
            }
        };

        // when
        WordCounter wordCounter = getWordCounterImplementation(repository);
        TestObserver<List<WordRecord>> testObserver = wordCounter.createObservable().test();

        // then
        testObserver.assertNoErrors();
        testObserver.assertValueCount(1);
        List<WordRecord> result = testObserver.values().get(0);
        Assert.assertEquals(1, result.size());
        Assert.assertEquals("word", result.get(0).getWord());
        Assert.assertEquals(1, result.get(0).getCount());
    }

    @Test
    public void testGivenTwoDifferentWordsReceived_shouldReturnThoseTwoWords() throws Exception {
        // given
        WordsRepository repository = new WordsRepository() {
            @Override
            public Observable<String> words() {
                return Observable.just("word1", "word2");
            }
        };

        // when
        WordCounter wordCounter = getWordCounterImplementation(repository);
        TestObserver<List<WordRecord>> testObserver = wordCounter.createObservable().test();

        // then
        testObserver.assertNoErrors();
        testObserver.assertValueCount(1);
        List<WordRecord> result = testObserver.values().get(0);
        Assert.assertEquals(2, result.size());

        Assert.assertEquals("word1", result.get(0).getWord());
        Assert.assertEquals(1, result.get(0).getCount());

        Assert.assertEquals("word2", result.get(1).getWord());
        Assert.assertEquals(1, result.get(1).getCount());
    }

    @Test
    public void testGivenThreeWordsReceivedAndOneIsDuplicated_shouldReturnTwoWords() throws Exception {
        // given
        WordsRepository repository = new WordsRepository() {
            @Override
            public Observable<String> words() {
                return Observable.just("word1", "word2", "word1");
            }
        };

        // when
        WordCounter wordCounter = getWordCounterImplementation(repository);
        TestObserver<List<WordRecord>> testObserver = wordCounter.createObservable().test();

        // then
        testObserver.assertNoErrors();
        testObserver.assertValueCount(1);
        List<WordRecord> result = testObserver.values().get(0);
        Assert.assertEquals(2, result.size());

        Assert.assertEquals("word1", result.get(0).getWord());
        Assert.assertEquals(2, result.get(0).getCount());

        Assert.assertEquals("word2", result.get(1).getWord());
        Assert.assertEquals(1, result.get(1).getCount());
    }

}
