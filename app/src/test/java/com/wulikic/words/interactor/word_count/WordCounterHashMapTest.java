package com.wulikic.words.interactor.word_count;

import com.wulikic.words.data.WordsRepository;

/**
 * Created by vesna on 29/07/2017.
 */
public class WordCounterHashMapTest extends WordCounterTest {

    @Override
    protected WordCounter getWordCounterImplementation(WordsRepository repository) {
        return new WordCounterHashMap(repository);
    }

}