package com.wulikic.words.interactor.prime_number;

import android.content.res.AssetManager;

import com.wulikic.words.data.Tokenizer;
import com.wulikic.words.data.TokenizerImpl;
import com.wulikic.words.data.WordsAssetsRepository;
import com.wulikic.words.entity.WordRecordWithPrimeAttribute;
import com.wulikic.words.interactor.word_count.WordCounterRx;
import com.wulikic.words.util.FileUtils;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import java.io.File;
import java.io.FileInputStream;
import java.util.List;

import io.reactivex.observers.TestObserver;

/**
 * Created by vesna on 28/07/2017.
 */
@RunWith(MockitoJUnitRunner.class)
public class WordCountAndPrimeNumberIntegrationTest {

    @Mock
    AssetManager assetManager;

    @Test
    public void test() throws Exception {
        // given
        String fileName = Mockito.anyString();
        File file = FileUtils.getResourceFile("some_text.txt");
        Mockito.when(assetManager.open(fileName)).thenReturn(new FileInputStream(file));
        Tokenizer tokenizer = new TokenizerImpl();
        WordCounterRx wordCounterImp = new WordCounterRx(new WordsAssetsRepository(assetManager, fileName, tokenizer));
        PrimeNumberChecker primeNumberChecker = new PrimeNumberCheckerImpl();
        PrimeWordDetectorImpl primeDetectionUseCase = new PrimeWordDetectorImpl(wordCounterImp, primeNumberChecker);

        // when
        TestObserver<List<WordRecordWithPrimeAttribute>> testObserver = primeDetectionUseCase.createObservable().test();

        // then
        testObserver.assertNoErrors();
        testObserver.assertValueCount(1);
        List<WordRecordWithPrimeAttribute> values = testObserver.values().get(0);

        Assert.assertEquals("i", values.get(0).getWordRecord().getWord());
        Assert.assertEquals(1, values.get(0).getWordRecord().getCount());
        Assert.assertFalse(values.get(0).isCountNumberPrime());

        Assert.assertEquals("the", values.get(1).getWordRecord().getWord());
        Assert.assertEquals(11, values.get(1).getWordRecord().getCount());
        Assert.assertTrue(values.get(1).isCountNumberPrime());

        Assert.assertEquals("of", values.get(3).getWordRecord().getWord());
        Assert.assertEquals(3, values.get(3).getWordRecord().getCount());
        Assert.assertTrue(values.get(3).isCountNumberPrime());
    }

}